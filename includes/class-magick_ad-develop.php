<?php
//开发用的测试数据展示类
if (!class_exists('Magick_ad_Develop')) {
    class Magick_ad_Develop
    {
        /**
         * 启动
         */
        public static function run()
        {
            //载入后台广告处理用文件
            require_once plugin_dir_path(__DIR__) . 'admin/partials/magick_ad-admin-cope.php';

            //载入前台广告处理用文件
            require_once plugin_dir_path(__DIR__) . 'public/class-magick_ad-public.php';

            //展示后台广告内容
            add_action('wp_footer', array(__CLASS__, 'config_ad'));

            //展示前台广告内容
            //add_action('wp_footer', array(__CLASS__, 'more_ad'));
        }

        /**
         * 数据展示方法
         */
        public static function p($data)
        {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        }


        /**
         * 将拿到的配置信息输出
         */
        public static function config_ad()
        {
            //准备处理函数
            $handle = new Magick_ad_Admin_Cope();
            //准备数据
            //全局广告
            $config = get_field('ad_all', 'options');
            //局部广告
            $partData = get_field('ad_part', 'options');


            $arr['ad_all'] = self::p("<h3>原始全局广告内容</h3>");
            //基本展示数据
            $arr['config'] = self::p($config);
            //说明文本
            $arr['msg'] = self::p("<h3>下面是处理过的</h3>");
            //处理过的数据
            $arr['handle'] = self::p($handle->run($config));

           //$arr['hr'] = self::p("<hr />");

           //$arr['ad_part'] = self::p("<h3>原始指定广告内容</h3>");
           //$arr['ad_part_data'] = self::p($partData);
           //$arr['msg_part'] = self::p("<h3>下面是处理过的</h3>");
           //$arr['handle_part'] = self::p($handle->run($partData));

            return $arr;
        }


        /**
         * 准备原始更多广告数据
         */
        public static function more_ad()
        {
            //准备数据
            $config = get_field('ad_more', 'options');
            echo '<hr/><h3>更多广告</h3>';
            //实例化用到的类
            $obj = new  Magick_ad_Public_More();
            //基本展示数据
            $arr['config'] = self::p($config);
            //说明文本
            $arr['msg'] = self::p("<h3>处理过的更多广告</h3>");
            //处理过的数据
            $arr['handle'] = self::p($obj->do($config));
        }
    } //end
}
