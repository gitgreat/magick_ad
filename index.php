<?php // Silence is golden




function add_type_attribute_to_script($tag, $handle)
{
    // 在这里判断需要添加 type 属性的 JS 文件，比如文件名包含 xxx.js
    if (strpos($tag, 'vite.js') !== false) {
        // 在 script 标签中添加 type 属性
        $tag = str_replace('<script', '<script type="module"', $tag);
    }
    return $tag;
}
add_filter('script_loader_tag', 'add_type_attribute_to_script', 10, 2);


/**
 * 获取当前页面hook
 */
function wpdocs_myselective_css_or_js($hook)
{
    echo '<h1 style="color: crimson;text-align: center;">' . esc_html($hook) . '</h1>';
}

//add_action( 'admin_enqueue_scripts', 'wpdocs_myselective_css_or_js' ); 











//搜索页添加广告
function add_hello_footer($posts)
{
    // 检查是否为搜索页面且有至少两个搜索结果
    if (!is_search() && count($posts) < 2) {
        return;
    }
?>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // 找到第一个和第二个 article 元素
            var firstArticle = document.querySelectorAll('article')[0];
            var secondArticle = document.querySelectorAll('article')[1];

            // 创建一个包含完整 HTML 结构的字符串
            var customText = '<article><footer class="entry-footer default-max-width">你好，' + new Date().toLocaleString() + '</footer></article>';

            // 使用 innerHTML 将包含 HTML 的字符串插入到第一个 article 元素之后，第二个 article 元素之前
            firstArticle.insertAdjacentHTML('afterend', customText);
        });
    </script>

<?php
}
add_action('wp_footer', 'add_hello_footer');





