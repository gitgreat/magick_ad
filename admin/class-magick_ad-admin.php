<?php

/**
 * 插件的特定于管理员的功能。
 *
 * @link       https://www.npc.ink
 * @since      1.0.0
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin
 */

/**
 * 插件的特定于管理员的功能。
 *
 * 定义插件名称、版本和两个示例挂钩，用于说明如何将特定于管理员的样式表和JavaScript放入队列
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin
 * @author     Mzue <1355471563@qq.com>
 */
class Magick_ad_Admin
{



	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * 初始化类并设置其财产。
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{
		//传值

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		//加载文件
		$this->load_dependencies();
		//运行文件
		$this->run();
	}

	/**
	 * 载入文件
	 */
	private function load_dependencies()
	{
		//载入 ACF 插件信息处理类
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/magick_ad-admin-plugin-meat.php';

		//载入广告处理类
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/magick_ad-admin-cope.php';

		//载入广告加载类
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/magick_ad-admin-doing.php';

		//载入广告展示统计类
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/magick_ad-admin-count.php';
		/**
		 * 测试下
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/magick_ad-admin-display.php';
	}

	/**
	 * 执行文件
	 */
	private function run()
	{
		//加载广告内容
		add_action('wp', array(__CLASS__, 'show_ad'));

		//广告统计数据展示
		Magick_ad_Admin_Count::run($this->plugin_name, $this->version);

		//后台执行
		if (is_admin()) {
			//添加菜单
			add_action('init', array(__CLASS__, 'add_option_menu_magick_ad'));

			//插件信息处理
			Magick_ad_Admin_Plugin_Meat::run();
		}
	}

	/**
	 * 将广告信息输出
	 */
	public static function show_ad()
	{

		//获取全局广告
		$configs = get_field('ad_all', 'options');
		//获取局部广告
		$partData = get_field('ad_part', 'options');

		//处理信息
		$data = Magick_ad_Admin_Cope::run($configs);
		$part = Magick_ad_Admin_Cope::run($partData);

		//加载广告
		Magick_ad_Admin_Doing::do($data);
		Magick_ad_Admin_Doing::do($part);
	}









	/**
	 * 添加广告菜单
	 */
	public static function add_option_menu_magick_ad()
	{
		//显示菜单
		//显示菜单选项
		//https://www.advancedcustomfields.com/resources/options-page/
		if (function_exists('acf_add_options_page')) {

			//acf_add_options_page();
			acf_add_options_page(array(
				'page_title' => '魔法广告',
				'menu_title' => '魔法广告',
				'menu_slug' => 'theme-general-settings',
				'capability' => 'manage_options', // 仅允许管理员访问
				'redirect' => false,
				'post_id' => 'options',
				'position' => '200.1',
				'icon_url' => 'dashicons-carrot',
				'update_button' => __('保存'),
				'updated_message' => __("保存成功"),
			));
		}
	}
}
