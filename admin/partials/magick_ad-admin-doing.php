<?php

/**
 * 添加位置列表
 * wp_head : 页面顶部
 * wp_footer : 页面底部
 * the_post : 文章或页面前（与循环前重复）
 * loop_start : 循环前
 * loop_end : 循环后
 * single_before : 文章顶部
 * single_three : 文章第三段
 * single_after : 文章底部
 * add_comment_text_before : 评论列表上方
 * comment_form_before : 评论框上方
 * comment_form_after : 评论框下方
 */
/**
 * 分类
 * category_description : 分类名下，分类描述广告
 */

/**
 * 标签
 * tag_description : 分类名下，分类描述广告
 */

/**
 * 将广告数组加载到页面
 *
 * 将传来的广告数组加载到页面
 *
 * @link       https://www.npc.ink
 * @since      1.0.0
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin/partials
 */

/**
 * 将广告数组加载到页面
 *
 * 将传来的广告数组加载到页面
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin/partials
 * @author     Mzue <1355471563@qq.com>
 */
if (!class_exists('Magick_ad_Admin_Doing')) {
    class Magick_ad_Admin_Doing
    {

        /**
         * 加载广告内容
         * @param array $config  包含内容的选项数组
         */
        public static function do($config)
        {
            // 判断传来的值是否是数组
            if (!is_array($config)) {

                return "数组无值或不存在! - Magick_ad_Admin_Doing";
            }

            foreach ($config as $my_content) {
                //拿到选项值
                $options = $my_content['option'];

                //拿到广告内容
                $ad_content = $my_content['content'];

                //拿到页面选项值（文章或页面、分类页、标签页、分类、标签）
                $condition = $options['show_page'];


                //拿到展示广告的位置，
                $position = self::get_position($options, $condition);

                //添加广告至页面
                self::judge_position($position, $ad_content);
            }
        }

        /**
         * 获取广告位置
         * @param string   $options   选项数组
         * @param string   $condition   页面选项值
         */
        private static function get_position($options, $condition)
        {
            //默认值
            $position = NULL;

            //判断是否存在singular_position，若不存在就是指定广告
            if (isset($options['singular_position'])) {

                //当前页面是否符合页面判断条件
                $result = self::judge_page($condition);

                //当前页面符合要求，输出位置
                if ($result) {
                    //是文章页或单页（广告位置较多）

                    if ($condition == "is_single" || $condition == "is_page") {
                        $position = $options['singular_position'];
                    } else {
                        $position = $options['show_position'];
                    }
                }
            } else {
                //指定广告
                /**
                 * 判断当前ID是否属于当前页面类型()
                 * 是的话输出位置，不然输出null
                 */

                //获取当前页面的ID（文章、页面、分类、标签）
                $id = get_queried_object_id();

                //拿到当前页面类型数组
                $page_arr =  $options[$condition]['data'];

                //是页面内容
                if ($condition == "singular" || $condition == "category" || $condition == "tag") {

                    //符合条件，输出位置
                    if (in_array($id, $page_arr)) {
                        $position = $options[$condition]['position'];
                    }
                }

                //是分类下、标签下内容
                if ($condition == "categorys" || $condition == "tags") {

                    $has_tag = false;

                    foreach ($page_arr as $tag_id) {

                        if (($condition == "categorys" && has_category($tag_id, $id)) ||
                            ($condition == "tags" && has_tag($tag_id, $id))
                        ) {
                            $has_tag = true;
                            break;
                        }
                    }

                    if ($has_tag) {
                        $position = $options[$condition]['position'];
                    }
                }
            }
            return $position;
        }


        /**
         * 判断当前页面是否符合条件
         * @param string   $condition   页面选项值
         */
        private static function judge_page($condition)
        {
            switch ($condition) {

                    //全站 - 主站点
                case 'is_main_site':
                    if (is_main_site()) {
                        return true;
                    }
                    break;

                    //仅首页
                case 'is_home':
                    if (is_home()) {
                        return true;
                    }
                    break;

                    //仅文章页
                case 'is_single':
                    if (is_single()) {
                        return true;
                    }
                    break;

                    //仅单页
                case 'is_page':
                    if (is_page()) {
                        return true;
                    }
                    break;

                    //仅分类页
                case 'is_category':
                    if (is_category()) {
                        return true;
                    }
                    break;

                    //仅标签页
                case 'is_tag':
                    if (is_tag()) {
                        return true;
                    }
                    break;

                    //仅搜索结果页
                case 'is_search':
                    if (is_search()) {
                        return true;
                    }
                    break;

                    //仅404页
                case 'is_404':
                    if (is_404()) {
                        return true;
                    }
                    break;

                    //仅作者页
                case 'is_author':
                    if (is_author()) {
                        return true;
                    }
                    break;

                    //默认值
                default:
                    return false;
                    break;
            }
        }


        /**
         * 执行广告添加任务
         * @param string   $position   位置
         * @param string   $ad_content   广告内容。
         */

        private static function judge_position($position, $ad_content)
        {
            switch ($position) {
                    //页面顶部
                case 'wp_head':
                    self::ad_wp_head($ad_content);
                    break;
                    //页面底部
                case 'wp_footer':
                    self::ad_wp_footer($ad_content);
                    break;

                    //页面循环前
                case 'loop_start':
                    self::ad_loop_start($ad_content);
                    break;

                    //页面循环后
                case 'loop_end':
                    self::ad_loop_end($ad_content);
                    break;

                    //文章/页面前
                case 'the_post':
                    self::ad_the_post($ad_content);
                    break;

                    //文章内容前
                case 'single_before':
                    self::ad_single_before($ad_content);
                    break;

                    //文章内容第三段
                case 'single_three':
                    self::ad_single_three($ad_content);
                    break;

                    //文章内容后
                case 'single_after':
                    self::ad_single_after($ad_content);
                    break;

                    //评论区列表上方
                case 'add_comment_text_before':
                    self::ad_add_comment_text_before($ad_content);
                    break;
                    //评论框上方
                case 'comment_form_before':
                    self::ad_comment_form_before($ad_content);
                    break;
                    //评论框下方
                case 'comment_form_after':
                    self::ad_comment_form_after($ad_content);
                    break;
                    //分类页-分类名下-分类描述
                case 'category_description':
                    self::ad_category_description($ad_content);
                    break;
                default:
                    break;
            }
        }





        /**
         * 页面顶部。
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_wp_head($ad_content)
        {
            $ad_callback = function () use ($ad_content) {

                echo $ad_content;
            };

            add_filter('wp_head', $ad_callback);
        }

        /**
         * 页面底部。
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_wp_footer($ad_content)
        {
            $ad_callback = function () use ($ad_content) {

                echo $ad_content;
            };
            // 在 wp_footer 钩子中添加广告回调函数
            add_filter('wp_footer', $ad_callback);
        }


        /**
         * 页面主循环前。
         *
         * 
         * @param string   $content   要显示的广告内容。
         */
        private static function ad_loop_start($ad_content)
        {

            $ad_callback = function () use ($ad_content) {
                //是主循环吗？
                if (is_main_query()) {

                    echo $ad_content;
                }
            };


            add_filter('loop_start', $ad_callback);
        }

        /**
         * 页面主循环后。
         *
         * 
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_loop_end($ad_content)
        {
            $ad_callback = function () use ($ad_content) {
                //是主循环吗？
                if (is_main_query()) {
                    echo $ad_content;
                }
            };


            add_filter('loop_end', $ad_callback);
        }


        /**
         * 文章或页面内容前-功能重复，废弃中。
         *
         * 
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_the_post($ad_content)
        {

            $ad_callback = function () use ($ad_content) {

                echo $ad_content;
            };


            add_filter('the_post', $ad_callback);
        }


        /**
         * 文章或页面的内容前
         *
         * 
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_single_before($ad_content)
        {

            $ad_callback = function ($content) use ($ad_content) {
                //保底输出，有则加，无则原
                $new_content = $content;

                $new_content = $ad_content . $content;

                return $new_content;
            };
            add_filter('the_content', $ad_callback);
        }

        /**
         * 文章或页面的内容第三段后
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_single_three($ad_content)
        {
            $ad_callback = function ($content) use ($ad_content) {
                //保底输出，有则加，无则原
                $res = $content;

                $paragraphs = explode('</p>', $content); // 按照 </p> 分割文章内容
                if (count($paragraphs) >= 3) { // 如果文章至少有 3 段
                    // 在第三段的结尾处添加自定义文本
                    $paragraphs[2] .= $ad_content;
                    // 将各段重新连接起来
                    $res = implode('</p>', $paragraphs);
                }

                return $res;
            };

            add_filter('the_content', $ad_callback);
        }

        /**
         * 文章或页面的内容后
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_single_after($ad_content)
        {
            $ad_callback = function ($content) use ($ad_content) {
                //保底输出，有则加，无则原
                $res = $content;

                $res =  $content . $ad_content;

                return $res;
            };
            add_filter('the_content', $ad_callback);
        }


        /**
         * 评论列表开始前
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_add_comment_text_before($ad_content)
        {

            $ad_callback = function () use ($ad_content) {
                //判断开启了评论且评论数大于0
                if (comments_open() && get_comments_number() > 0) {
                    echo $ad_content;
                }
            };



            add_filter('comments_template', $ad_callback);
        }


        /**
         * 评论框开始前
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_comment_form_before($ad_content)
        {
            $ad_callback = function () use ($ad_content) {
                //是否开启了评论框
                if (comments_open()) {

                    echo $ad_content;
                }
            };


            add_filter('comment_form_before', $ad_callback);
        }


        /**
         * 评论框开始后
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_comment_form_after($ad_content)
        {
            $ad_callback = function () use ($ad_content) {
                if (comments_open()) {
                    echo $ad_content;
                }
            };


            //开启评论的
            add_filter('comment_form_after', $ad_callback);
        }


        /**
         * 分类页的分类描述后
         *
         * @param string   $ad_content   要显示的广告内容。
         */
        private static function ad_category_description($ad_content)
        {

            $ad_callback = function ($content) use ($ad_content) {
                //保底输出，有则加，无则原
                $new_content = $content;

                $new_content =  $content . $ad_content;

                return $new_content;
            };
            add_filter('category_description', $ad_callback);
        }
    } //end
}
