<?php

/**
 * 为插件提供管理区域视图
 *
 * 该文件用于标记插件的面向管理的方面。
 *
 * @link       https://www.npc.ink
 * @since      1.0.0
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin/partials
 */

/**
 * 插件的管理员的功能。
 *
 * 执行一些动作
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin/partials
 * @author     Mzue <1355471563@qq.com>
 */
if (!class_exists('Magick_ad_Admin_Display')) {
  class Magick_ad_Admin_Display
  {


  } //end
}
