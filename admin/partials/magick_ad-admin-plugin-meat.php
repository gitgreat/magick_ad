<?php

/**
 * 修改插件信息
 *
 * 对 ACF 插件展示的信息进行修改
 *
 * @package    Magick_ad
 * @subpackage Magick_ad/admin
 * @author     Mzue <1355471563@qq.com>
 */
if (!class_exists('Magick_ad_Admin_Plugin_Meat')) {
    class Magick_ad_Admin_Plugin_Meat
    {
        public static function run()
        {
            //屏蔽更新提示
            add_filter('site_transient_update_plugins', array(__CLASS__, 'wcr_remove_update_notifications'));

            //添加自定义提示
            add_filter('plugin_row_meta', array(__CLASS__, 'my_plugin_add_meta'), 10, 2);

            //添加广告插件设置信息
            add_filter('plugin_action_links_magick_ad/magick_ad.php', array(__CLASS__, 'add_option_setting'));

            //添加广告插件附加信息
            add_filter('plugin_row_meta', array(__CLASS__, 'add_option_msg_setting'), '', 2);
        }

        /**
         * 屏蔽ACF Pro 插件更新提示
         *
         * @type    function
         * @date    14/06/2016
         * 
         * @since    1.0.0
         */
        public static function wcr_remove_update_notifications($value)
        {
            // 要屏蔽的插件位置 (在wp-content/plugins文件夹下)
            $plugins = array(
                'advanced-custom-fields-pro/acf.php',
            );
            foreach ($plugins as $key => $plugin) {
                if (empty($value->response[$plugin])) {
                    continue;
                }
                unset($value->response[$plugin]);
            }
            return $value;
        }

        /**
         * 添加提示信息
         */
        public static function my_plugin_add_meta($links, $file)
        {
            // Replace "my-plugin/my-plugin.php" with your plugin's file path
            if ($file == 'advanced-custom-fields-pro/acf.php') {
                $notice = '<br><span style="color: #8c8c8c;">提示：若需停用本插件，请先停用魔法广告插件</span>';
                $links[] = $notice;
            }
            return $links;
        }

        /**
         * 添加本插件设置信息
         */
        public static function add_option_setting($links)
        {
            $links[] = '<a href="' . get_admin_url(null, 'admin.php?page=theme-general-settings') . '">' . __('设置', 'tj') . '</a>';
            return $links;
        }
        /**
         * 添加本插件信息
         */
        public static function add_option_msg_setting($links, $file)
        {
            //增加插件信息

            //if ($file == plugin_basename(__FILE__)) {
            if ($file == 'magick_ad/magick_ad.php') {

                $links[] = '<a target="_blank" href="https://www.npc.ink/276641.html">使用帮助</a>';
                $links[] = '<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=1355471563">联系QQ</a>';
            }
            return $links;
        }
    } //end
}
